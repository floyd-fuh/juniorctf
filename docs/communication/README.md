
  - [Lightning talk](https://slides.com/invisibleman/juniorctf/fullscreen#/) at [Hack.Lu](https://2018.hack.lu) 2018
  - [Install party](https://slides.com/invisibleman/juniorctf-1#/) workshop at [Hack.lu](https://2019.hack.lu) 2019


# Logos

Logo number 2 got selected :)

![](./logo1.png)
![](./logo2.png)	
![](./logo3.png)	
