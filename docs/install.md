## Requirements - current limitations

Your kids will need access to:

- SSH client
- a terminal
- an editor
- John The Ripper (I'll try and remove that requirement later)
- Internet. Several challenges don't strictly require Internet, but it might be helpful to find some documentation.

Current limitation: challenges are in **French** currently. But I'm sure it won't be too difficult to translate! 

## Setting up your own Junior CTF

To setup your own Junior CTF, you need:

- **This repository**. It contains the scenarios & sources for the challenges.
- A **CTF framework** to validate the flags, handle scores. And perhaps hints, teams etc. I personally use [CTFd](https://github.com/CTFd/CTFd). I'm sure you can get it to work with something else.
- **Docker**. Each challenge runs in its own Docker container.
- **docker-compose**

### Prepare CTFd

Install CTFd. We recommend you [follow the install guidelines](https://github.com/CTFd/CTFd).
Depending on whether this server will be local to your home or public, read the different deployment procedures.
Also, of course, you can select the port for the CTFd web interface. If you want to use ports lower than 1024, you'll need adequate rights to do so on your OS.

As of Oct 8 2019, the steps to perform a basic install of CTFd are the following:

1. Clone the repository: `git clone https://github.com/CTFd/CTFd`
2. Install requirements: `pip install -r requirements.txt`
3. Edit `docker-compose.yml` to your likes. In particular, the CTFd port number and MySQL root and user passwords
4. Build and launch the containers with `docker-compose up -d`

At this stage, CTFd is operational, but not configured and empty.

### Prepare Junior CTF challenges

Now, let's prepare Junior CTF challenges:

1. Compile necessary files: run `make all`
2. Build and run all Junior CTF containers: run `docker-compose up -d`.
3. Finally, import your challenges in your CTF framework. For CTFd, we have a script in `./sysadmin`, run `python ./import.py --basedir BASEDIR -u WEBURL -l admin -p PASSWORD --setup --email AVALIDEMAIL --lang YOURLANG`

For example,

```
python ./import.py --basedir /home/axelle/juniorctf/ -u http://myjunior.ctf.info/ -l admin -p XXXXXX --setup --email avalidemail@blah.com --lang en
```

The `--setup` option creates an admin account on CTFd for you.

