Title: Miss Moneypenny
Category: Passwords
Score: 100
Description:

<<<<<<< HEAD
James, Miss Moneypenny ist tief im Schwarzwald in Urlaub gegangen. Das ist dumm, dort haben sie keinen Empfang, und ich komme nicht zu ihr durch...

Jetzt brauche ich eine Kopie der Operation "Fly in the Sky" von Miss Moneypenny ihrem Computer.

Können Sie bitte auf ihren Computer zugreifen?

## Zugriff auf Moneypennys Computer ##

Sie greifen über SSH darauf zu:

- Port 2722.
- Benutzername: `Geldpenny`.
- die Server-Adresse lautet `192.168.0.8`.


## Ooh, aber welches Passwort?

Es ist unnötig zu sagen, dass Miss Moneypenny eine Schwäche für Sie hat, James.
=======
James, Miss Moneypenny macht aktuell Urlaub in den Bündner Bergen. Das ist auch schlau, denn dort oben hat man keinen Empfang. Daher komme ich auch nicht zu ihr durch...

Ich brauche aber dringend eine Kopie der Operation "Fly in the Sky" von ihrem Computer.        

Kannst du bitte auf ihren Computer zugreifen?

## Zugriff auf den Computer von Moneypenny

Du greifst über SSH zu:

- Port: `2722`
- Benutzername: `moneypenny`
- die Server Adresse lautet `192.168.0.8`.


## Wie lautet das Passwort?

Unnötig zu sagen, dass Miss Moneypenny eine Schwäche für dich hat, James.
>>>>>>> cc920e73e931d461ecee81ebd075e10ca46bc067

Wir wissen, dass ihr Passwort aus dem Vornamen *oder* dem Nachnamen eines James-Bond-Schauspielers besteht, gefolgt von dem **Nachnamen** wiederum eines James-Bond-Schauspielers.

Wenn zum Beispiel Bruce Willis und Harrison Ford James-Bond-Schauspieler wären, könnte das Passwort lauten:

- brucewillis
- bruceford
- harrisonwillis
- harrisonford
- williswillis
- willisford
- fordwillis
- fordford

Das Passwort ist in ** Kleinbuchstaben, ohne Leerzeichen oder Akzente**.

<<<<<<< HEAD
## Bearbeiten Sie die Datei john.conf

Auf [einem Forum, das den Benutzern von John The Ripper gewidmet ist] (http://www.openwall.com/lists/john-users/2008/10/17/2) finden Sie die folgenden Informationen:

```
Sie müssten Ihre "Ausgangswörter" in eine Wortlistendatei einfügen, eines pro Zeile:

        Katze
        Hund
        Vogel
        Kuh

und Sie müssen Wortlistenregeln aus Ihren "zweiten" Wörtern erstellen, indem Sie den Befehl "Zeichen anhängen" verwenden:

        [Liste.Regeln:Wortliste]
        $c$a$t
        $d$o$g
        $b$i$r$d
        $c$o$w
```

Das ist nicht ganz derselbe Fall, denn es geht nicht um Tiere, sondern um Vornamen/Schauspielernamen. Aber die Idee ist die gleiche. James, Sie würden ja nicht wollen, dass ich die Arbeit für Sie mache?

## Zeigen Sie die von Ihrer john.conf generierten Passwörter

Um zu überprüfen, ob deine`john.conf'-Datei korrekt ist, musst Du möglicherweise alle Passwörter anzeigen, welche John prüft.

Verwenden Sie dazu den Befehl: `john --wordlist=words --rules --stdout`.
Ersetzen Sie `Wörter` durch den Namen Ihrer Wortdatei.

## Haben Sie Ihr Gedächtnis verloren, James?

Wenn Sie sich nicht mehr erinnern können:

- John The Ripper laufen lassen
- Kopieren Sie die Datei john.conf an die richtige Stelle
- usw.

schauen Sie in John 2!
=======
## Bearbeite die Datei john.conf

Auf [einem Forum, das den Benutzern von John The Ripper gewidmet ist] (http://www.openwall.com/lists/john-users/2008/10/17/2) findest du die folgenden Informationen:

```
you'll need to place your "first" words into a wordlist file, one per line:

	cat
	dog
	bird
	cow

and you need to create wordlist rules out of your "second" words, using
the "append character" command:

	[List.Rules:Wordlist]
	$c$a$t
	$d$o$g
	$b$i$r$d
	$c$o$w
```

Das ist nicht ganz derselbe Fall, denn es geht nicht um Tiere, sondern um Vornamen/Nachnamen von Schauspielern. Aber die Idee ist die gleiche. James, du würdest auch nicht wollen, dass ich die Arbeit für dich mache?  
## Zeigen Sie die von Ihrer john.conf generierten Passwörter an

Um zu überprüfen, ob Ihre `john.conf'-Datei korrekt ist, musst du möglicherweise alle Passwörter anzeigen, welche generiert werden.

Verwende dazu den Befehl: `john --wordlist=words --rules --stdout`.
Ersetzen Sie `words` durch den Namen Ihrer Wörterdatei.

## Hast du dein Gedächtnis verloren, James?

Wenn du dich nicht mehr erinnern kannst:

- starte John The Ripper
- kopiere die Datei john.conf an die richtige Stelle
- usw.

schaue in John 2 nach!
>>>>>>> cc920e73e931d461ecee81ebd075e10ca46bc067

