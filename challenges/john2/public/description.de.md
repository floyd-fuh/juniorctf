Title: John 2
Score: 100
Category: Passwörter
Description:

Palpaguin bekam Wind von einem Einbruch in sein Kontrollsystem und änderte sofort sein Passwort. Diesmal wissen wir, dass es sich um eine Frucht auf Englisch handelt, gefolgt von einer Zahl.

Schaffst du es, dich wieder an der Maschine anzumelden?
Du hast ebenfalls wieder die *hashed* Passwortdatei zum Herunterladen.

Du benötigst ebenfalls eine `john.conf` Konfigurationsdatei (herunterladbar). Ich werde im Folgenden erklären, warum.

## Warnung

1. Diese Veranstaltung ist schwieriger als `John 1`. Ich empfehle dir, zuerst John 1 zu machen.
2. Blah bla, du weißt, dass du im wirklichen Leben nur Passwörter von Maschinen knackst, die dir gehören. **Von niemandem sonst**.


## Zugriff auf das Kontrollsystem

Der Zugriff erfolgt per SSH auf Port 2122. Palpaguins Benutzername ist `palpaguin` und die Serveradresse ist `192.168.0.8`.

Bist du ein SSH-Profi? Das sollte ausreichen, damit du auf das System zugreifen kannst. Wenn du Probleme hast, überprüfe die Hilfe in **john1** noch einmal.


## Passwörter mit John knacken.

Ich schlage vor, dass du wieder John The Ripper benutzt. Aber diesmal musst du klüger oder sehr geduldig sein. Was immer du bevorzugst...

Wenn du den Mut hast, eine Liste von Früchtenamen, gefolgt von Zahlen, zu schreiben, kannst du das tun.

Aber bei John geht es auch etwas subtiler und einfacher. Du kannst ihm eine Liste mit grundlegenden Wörtern geben und ihm dann sagen, dass er sie ein wenig ändern soll. Das nennt man eine Regel einsetzen, auf Englisch 'rules'.

### Sichern der Konfigurationsdatei

Die Regeln musst du in eine Konfigurationsdatei schreiben, die du in `/etc/john/john.conf` einträgst. Ich empfehle dir, zuerst die bestehende Datei zu speichern. In einem Terminal tippst du anschliessend:

```
sudo cp /etc/john/john.conf /etc/john/john.conf.bak
```

### Sudo

`sudo`, das ist ein Befehl, den du schon einmal gesehen hast. Er ist wichtig, er erlaubt dir Aktionen durchzuführen, für die du ein Administrator auf deinem Computer sein müsstest (unter Linux nennt man das `root`). Um einen Befehl als `root` auszuführen, kann man einfach `sudo` davorschreiben (falls man die entsprechende Berechtigung hat).

Du hast grundsätzlich nicht das Recht, in die Unterverzeichnisse von `/etc` zu schreiben. Du bist also verpflichtet, `sudo' zu benutzen.

### Verstehen der Konfigurationsdatei

Erst einmal kopieren wird die von mir erstellte Regeldatei an die richtige Stelle. Bevor du das tust, schau es dir erst einmal an. Die wichtige Regel ist die folgende:

```
# Fügen Sie eine Zahl an
Az" [0-9]"
```

Das heisst:

1. Kopiere in der Wortliste alle Zeichen von A bis Z und von a bis z. Wenn das Wort beispielsweise "Apfel" ist, sind alle Zeichen zwischen a und z, also werden sie alle kopiert: "Apfel".
2. Füge anschliessend eine Ziffer zwischen 0 und 9 an.

### Konfigurationsdatei kopieren

Kopier die Datei `john.conf`, die du heruntergeladen hast nach `/etc/john/john.conf`:

```
sudo cp john.conf /etc/john/john.conf
```

Damit dieser Befehl funktioniert, musst du dich in dem Verzeichnis befinden, in dem du die Datei `john.conf' heruntergeladen hast. Du kannst das Verzeichnis mit dem Befehl `cd` ändern.

### John ausführen

Anschliessend starte John. Dazu musst du in einem Terminal eingeben:

```
john mdphache --wordlist=fruitlist --regeln
```

Vorsicht, es liegt an dir, eine schöne Fruchtliste zu schreiben, auf Englisch.
Viel Glück!

Übersetzt mit www.DeepL.com/Translator (kostenlose Version)
