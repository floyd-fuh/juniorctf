Title: Moyenne
Category: Programmieren
Score: 100
Description:

Pico hat seine Noten bekommen. Diese sind:

- 19/20
- 19/20
- 17/20
- 11/20 (was für ein Schwachkopf)
- 17/20
- 18/20

Wir müssten den Durchschnitt bilden. Der Durchschnitt ist die *Summe* aller Noten (18, 19 ...) geteilt durch die *Anzahl der Noten*.

Sie müssten ein C-Programm erstellen, das den **Durchschnitt** von Pico berechnet.

Um die Punkte der Aufgabe zu erhalten, musst du **den Wert des von Ihrem Programm angezeigten Durchschnitts** abgeben.

## Anfang des Programms

Ein Programm muss wissen, wo es *anfangen* soll. Dies wird in der Programmierung oft als *Main* bezeichnet. In C sieht das so aus:

```C
void main() {
     /* hier schreibst du dein Programm */
}
```

Du wirst dein Programm dort schreiben, wo oben im Beispiel `/* hier schreibst du dein Programm */` ist.

Übrigens, beachten dass ein Kommentar mit `/*` beginnt und mit `*/` endet. Du kannst so viele Kommentare in den Code einfügen, wie du willst. Sie werden dir helfen, den Code (vorallem später) besser zu lesen und zu verstehen.

## Array 

Ein Array ist eine geordnete Liste, beispielsweise eine Liste von Zahlen. Am besten stellst du dir unter einem Array eine Art Tabelle vor.
Wir werden die Noten in einen Array aus Zahlen einfügen.

Um das zu tun, musst du den Array *deklarieren*, d.h. im Programm sagen: "Ich werde einen Array mit ganzen Zahlen verwenden".
So wird das gemacht:

```C
int array[anzahlElemente];
```

`anzahlElemente` ist die Anzahl der Elemente, die in das Array eingefügt werden sollen. Ersetze es durch 4, 5, 6 oder eine beliebige andere Zahl.

### Was ist in dem Array

Du kannst in deinem Programm wie folgt auf die Inhalte des Arrays zugreifen:

- `array[0]`: erster Wert des Arrays / erste Zelle der Tabelle
- `array[1]`: zweiter Wert des Arrays / zweite Zelle der Tabelle
- `array[2]`: dritter Wert des Arrays / dritte Zelle der Tabelle
- ...

Achtung, beachte dass das erste Feld **Null** ist. Denn Computer fangen immer bei 0 an zu zählen.
Ausserdem kannst du nicht auf ein Element des Arrays zugreifen, das nicht existiert. Wenn du zum Beispiel einen Array mit fünf Elementen definiert hast (`int array[5];`), dann kannst du nicht auf das zehnte Element `array[9]` zugreifen.

Du kannst wie folgt ein Element im Array zu setzen:

```C
array[0] = 10;
```

Dadurch wird der Wert 10 im ersten Element des Arrays gesetzt.

## Variablen
Ein fundamentales Konzept jeder Programmiersprache sind Variablen. Variablen können Werte enthalten (zum Beispiel Text oder Zahlen) und haben einen Namen über den man wieder darauf zugreifen kann.
Unser Array ist beispielsweise auch eine Variable.

## Berechnungen ##

### Addition

Um zwei Zahlen zu addieren, verwenden wir `+`. 

```C
i = 2 + 4;
```
Welchen Wert hat das `i`? Genau, 6.


Sie können auch Variablen bzw. Inhalte von Variablen addieren.

```C
int i = 1;
int j = 2;
int z = i +j +3;
```

In diesem Beispiel ist z gleich 6.

### Division

**Divisionen** sind etwas komplizierter, denn wenn man etwas teilt, erhält man nicht unbedingt ein ganzes Ergebnis (bzw. eine ganze Zahl).

Daher verwendest du am besten für Divisionen einen `float` (Zahl mit Nachkommastellen) und nicht ein `int` (Zahl ohne Nachkommastellen).

Deklaration der Variable durchschnitt als float:

```C
float durchschnitt;
```

## Ausgabe / Anzeigen

Um etwas auf dem Bildschirm anzuzeigen, z.B. das Ergebnis des Durchschnitts, kannst du `printf` verwenden.

So kannst du eine ganze Zahl ausgeben:

```C
printf('Wert: %d\n', array[2]);
```

- Du kannst `Wert` durch alles ersetzen, was du willst. Das ist nur Text.
- Das `%d` ist wichtig und bedeutet "hier einen ganzzahligen Wert setzen".
- Das `\n` bedeutet, zur nächsten Zeile zu gehen.


`printf` ist eine vordefinierte *Funktion* (d.h. jemand hat das Programm für dich geschrieben, das dem Computer sagt, dass er Dinge auf dem Bildschirm anzeigen soll). Um sie benutzen zu können, musst du am Anfang deines Programms folgendes einsetzen (genau so):

```C
#include <stdio.h>
```

Wenn du eine Zahl mit Komma ausgeben willst, kannst du `%f` anstatt `%d` verwenden.

## Die ärgerlichen Dinge beim C programmieren


### Semikolon?

- Alle Anweisungen müssen mit einem Semikolon `;` enden. Zum Beispiel:

```
`C
array[0] = 10;
```


Beachte, dass in einigen Fällen kein Semikolon zum Einsatz kommt. Wie zum Beispiel hier:

```C
void main() { /* kein ; */
     i = 2;
} /* kein ; */
```


### Gross- und Kleinschreibung

Beim Programmieren ist die Gross- und Kleinschreibung wichtig. 
Die `Pinguin`-Variable ist nicht dieselbe wie die `pinguin`-Variable.

```C
int pinguin = 2;
int Pinguin = 5;
```

### Leerzeichen

In Variablennamen kannst du keine Leerzeichen verwenden. 

Du kannst `_` verwenden, aber nicht `-`. 

Im Programm selbst hingegen kannst du (fast) überall Leerzeichen einfügen:

```C
void main() {
     

     i = 4;
}
```
Es ist dasselbe wie:

```C
void main() {
     i = 4;
}
```
Und immer noch dasselbe wie (aber wir vermeiden diese Version):

```C
void                   main() {
     i =      4;
}
```

## Kompilieren

Ihr Programm ist für einen Computer nicht verständlich.
Es muss übersetzt werden. Dafür musst du im Terminal tippen:

```
gcc prog.c -o prog
```

- prog.c: das ist der C-Dateiname. Wenn dein Programm anders heisst musst du den Namen an dieser Stelle einfügen.
- prog: Dies ist der endgültige Name der Datei, die der Computer versteht und die du dann ausführen kannst. Auch diesen Namen kannst du ändern.

Um das Programm dann auszuführen, tippst du:

```
./prog
```

Wenn alles gut geht, wird es funktionieren. Wenn nicht, zeigt dir dein Computer Fehlermeldungen.
Wenn es Fehler gibt, ist das nicht schlimm! Das passiert **allen Programmierern** und jeder hat so begonnen. 
