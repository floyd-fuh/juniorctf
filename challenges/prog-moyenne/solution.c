#include <stdio.h>

void main() {
  int tabelle[6];

  tabelle[0] = 19;
  tabelle[1] = 19;
  tabelle[2] = 17;
  tabelle[3] = 11;
  tabelle[4] = 17;
  tabelle[5] = 18;

  float durchschnitt;
  int i = 0;
  int summe = 0;

  for (i=0; i<6; i++)
    summe = tabelle[i] + summe;

  durchschnitt = (float) summe / 6;

  printf("durchschnitt : %f\n", durchschnitt);
}

