Titel: Sehr geheime Basis 3
Kategorie: Reverse
Punktzahl: 200
Beschreibung:


Hallo Hacker! Versuche das Programm `getin3` zu knacken. 

- Lade es vom CTF-Server herunter (siehe unten). 
- Gehe in das Verzeichnis, in das du die Datei heruntergeladen hast (erinnerst du dich? benutze den Befehl `cd`).
- Mache die Datei ausführbar.
- Führe es schließlich aus: `./getin3`
- Jetzt kannst du versuchen, das Passwort zu erraten... Viel Erfolg!
- Wenn du kurz vor der Idee stehst kannst du versuchen, das zu tun, was man *reversing* nennt. Das bedeutet, dass du die ausführbare Datei mit einem speziellen Werkzeug, einem sogenannten Disassembler, studierst und versuchst, zu verstehen, was es macht.



## Auflisten von Zeichenfolgen in einer ausführbaren Datei

Du solltest in einer früheren `getin'-Herausforderung gelernt haben, wie man dies macht ;-)

Leider wirst du das Passwort in diesem Fall nicht auf diese Weise erkennen, da die Zeichenkette *verschlüsselt* wurde.

Was ist Verschlüsselung? Im Grunde ist es etwas, das eine Nachricht in etwas verwandelt, das niemand verstehen kann... außer den Personen, die wissen, wie man es macht und/oder einen Schlüssel dafür haben.


## Radare 2 ausführen

Radare2 ist ein *Disassembler* (es gibt aber auch andere Disassembler). 
Es versucht, ausführbaren Dateien für Menschen verständlich zu machen. Programme sind wenn sie *compiled* (also übersetzt) werden, für Menschen nicht mehr gut Lesbar. 
Deshalb braucht man ein Disassembler um eine ausführbare Datei zu verstehen.

### Haben Sie Radare2?

Öffne ein Terminal. Gib dann `r2 -v` ein. Wenn du eine Antwort wie "command not found" erhaltest, musst du [Radare2](https://www.radare.org/r/down.html) installieren: **einen Erwachsenen bitten, das für dich zu tun**.

### *Disassemble* die ausführbare Datei

Der Befehl von Radare2 ist die Abkürzung `r2`.

Führe den befehl `r2 ./getin3` aus.

```
$ r2 ./getin3
[0x00400660]>
```

Die Sache zwischen den Klammern ist die *Adresse*. Radare versteht ein paar Befehle. Versuche zum Beispiel mit `aa` zu sagen, es solle "alles analysieren".

```
x] Analysiere alle Flags, die mit sym. und Eintrag0 (aa) beginnen.
```

Sagen Sie ihm nun, er soll alle Funktionen mit dem Befehl `afl` auflisten:

```
[0x00400660]> afl
0x00400598 3 26 sym._init
0x004005e0 2 16 -> 48 sym.imp.strlen
0x004005f0 2 16 -> 48 sym.imp.__stack_chk_fail
0x00400600 2 16 -> 48 sym.imp.printf
0x00400610 2 16 -> 48 sym.imp.strcspn
0x00400620 2 16 -> 48 sym.imp.__libc_start_main
0x00400630 2 16 -> 48 sym.imp.fgets
0x00400640 2 16 -> 48 sym.imp.strcmp
0x00400650 2 16 -> 48 loc.imp.__gmon_start__
0x00400660 1 41 Eintrag0
0x00400690 4 50 -> 41 sym.deregistrieren_tm_klone
0x004006d0 3 53 sym.register_tm_klone
0x00400710 3 28 sym.__do_global_dtors_aux
0x00400730 4 38 -> 35 Eintrag1.init
0x00400756 10 266 hauptsächlich
0x00400860 4 101 sym.__libc_csu_init
0x004008d0 1 2 sym.__libc_csu_fini
0x004008d4 1 9 sym._fini
```

Du kannst Hilfe zu einem Befehl erhalten, indem du `?` verwendest. So gibt beispielsweise `af?` Hilfe zu allen Befehlen aus, die mit `af` beginnen.

### *Disassemble*

Der Hauptteil eines Programms ist sein Einstiegspunkt. Ein irgendwie wichtiger Teil, oder?

Wir werden es zerlegen: `pdf @ main`

Autsch, man bekommt eine Menge hässlichen Code :)

Es ist kein problem, wenn du beim ersten Mal nicht alles verstehen solltest. Versuchen wir es erst einmal leicht.

Dieser teil gibt die Nachricht "This is a very secret base" aus.

```
           0x00400786      bfe8084000     mov edi, str.____This_is_a_very_secret_base____ ; 0x4008e8 ; "=== This is a very secret base ==="
|           0x0040078b      e840feffff     call sym.imp.puts           ; loc.imp
```

Dieser teil gibt die Nachricht "Password: " aus.

```
|           0x00400790      bf0b094000     mov edi, str.Password:      ; 0x40090b ; "Password: "
|           0x00400795      b800000000     mov eax, 0
|           0x0040079a      e861feffff     call sym.imp.printf         ; int printf(const char *format)
```

Dieser Teil liest höchstens 25 Zeichen, die Sie eingeben. Im Grunde genommen ist `fgets` eine Funktion zum Lesen einer Zeichenkette. Und `stdin` bedeutet, dass Sie von der Eingabe (das was Sie eingeben) lesen.
```
|           0x0040079f      488b15ca0820.  mov rdx, qword [obj.stdin]  ; loc.imp. ; [0x601070:8]=0
|           0x004007a6      488d45c0       lea rax, [local_40h]
|           0x004007aa      be19000000     mov esi, 0x19               ; 25
|           0x004007af      4889c7         mov rdi, rax
|           0x004007b2      e879feffff     call sym.imp.fgets          ; char *fgets(char *s, int size, FILE *stream)
```

///////////////////////////////////////UNÜBERSETZT//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




Nun, wir werden hier sehr viel vereinfachen, aber diese Zeile fügt einem Zeichen Ihrer Eingabe eine 1 hinzu:

```
|     :||   0x004007e5      83c001         add eax, 1
```

Wir werden nicht jede Zeile des *mains* umkehren, weil Sie sich vor dem Ende langweilen würden ;-) Glauben Sie mir einfach, in diesem Fall ist das Programm:

1. Lesen Ihrer Eingabe
2. Hinzufügen von 1 zu jedem Zeichen Ihrer Eingabe
3. Der Vergleich mit einem *verschlüsselten* Passwort

### Einem Zeichen eine 1 hinzufügen?!

Computer verstehen Zeichen als Zahlen. Zum Beispiel ist `A` 65, `B` 66, `C` 67 usw. Diese Umwandlung wird als ASCII-Tabelle bezeichnet.

Nehmen wir also an, Sie haben das Zeichen `A`. Wenn wir 1 hinzufügen, ergibt dies 65+1=66. Dies entspricht also `B`. Okay?

### Wo ist das verschlüsselte Passwort?
Es befindet sich am Anfang der Hauptstrecke in den `0x63667652`...

```
| 0x00400775 c745b0527666. mov dword [local_50h], 0x63667652
| 0x0040077c 66c745b46664 mov word [local_4ch], 0x6466
```

Vorsicht, 0x63 bedeutet *63 zur Basis 16*. Sie verstehen vielleicht nicht, was das bedeutet, aber diese 63 ist nicht 63, sondern 99, und das entspricht eigentlich dem Zeichen `c`.

Wir werden radare2 bitten, dies für uns umzuwandeln:

- Geben Sie `s 0x00400775+3` ein.
- Dann geben Sie `px 6` ein.

```
[0x00400778]> px 6
- Versatz - 0 1 2 3 4 5 6 7 8 9 A B C D E F 0123456789ABCDEF
0x00400400778 5276 6663 66c7 Rvfcf.
```

Dies zeigt die Zeichendarstellung für (0x) 52 76 66 63 66: `Rvfcf`.

### Entschlüsseln Sie das Passwort

Also ist `Rvfcf` das verschlüsselte Passwort. Wir wissen, dass wir zu jedem Zeichen der Eingabe 1 hinzufügen. Um zu entschlüsseln, machen wir also das Gegenteil: wir subtrahieren 1 zum verschlüsselten Passwort.

- Welcher Buchstabe steht im Alphabet vor dem R?
- Welcher Buchstabe steht im Alphabet vor v?
- usw.

Sie erhalten das Passwort! Versuchen Sie es, und es sollte funktionieren. Benutzen Sie das Passwort, um diese Herausforderung zu markieren.



