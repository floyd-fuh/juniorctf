Title: SPECTRE Gegenangriff
Category: Netzwerk
Score: 60
Description:

Hallo, Nummer 10.

Hier Nummer 1.
"Miau." Und das ist meine fiese Siamkatze.

Wenn Sie wirklich in [S.P.E.C.T.R.E.](https://en.wikipedia.org/wiki/SPECTRE) einsteigen wollen, müssen Sie sich beweisen. Wir nehmen nicht jeden!

Ihre Mission ist sehr einfach. Sie müssen in die MI6-Server eindringen. Ja, das ist es. Und zu allem Überfluss werden Sie mit dem Login und Passwort von James Bond, unserem Todfeind, in den MI6 einloggen.

Wir haben den Datenverkehr zu ihren Servern überwacht. Laden Sie die Datei am Ende dieser Erklärung herunter.

# Verbindung zu den Servern des MI6

Sie können auf [das Web mit Ihrem Lieblingsbrowser] (http://ADRESSE_IP_MACHINE:8080) zugreifen.

# Wireshark

Um die Datei `net1.pcapng` zu lesen, benötigen Sie **Wireshark**.

Installieren Sie es auf Ihrem Rechner. Auf Ubuntu/Mint zum Beispiel mit `sudo apt-get install wireshark`.

Öffne dann die Datei `net1.pcapng` in Wireshark. Über den Terminal geht das mit dem Befehl `wireshark net1.pcapng`. 

Du siehst eine grafische Oberfläche, die dir die Netzwerkpakete zeigt, d.h. die Informationen, die zum MI6 geflossen sind und die SPECTRE abgefangen hat. Du kannst auf die Zeilen oben klicken und siehst unten weitere Details. Schaue dir jede Zeile genau an. Versuchen herauszufinden, was los ist, und vor allem versuche, den James-Bond-Login (Benutzername) und das zugehörige Passwort zu bekommen.

NB. Dies wäre mit *httpS* nicht möglich.
