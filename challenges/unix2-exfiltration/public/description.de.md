Title: Exfiltration
Category: Unix
Score: 50
Description:

James, hör mir zu. Auf dem Computer von Boris Grishenko sind die Pläne für Golden Eye.
Ich möchte, dass du sie zu mir bringst, damit Großbritannien diese Geheimwaffe studieren kann.

Sie werden die geheime Plandatei in das Verzeichnis /mi6 kopieren.
Um diese Aufgabe zu erfüllen, musst du den folgenden Befehl in einem Terminal ausführen:

`(ls /mi6 && cat /mi6/plans-secrets) | sha1sum`

Es wird so etwas wie die folgende Zeile zurückkommen:

```
45c9646a8342d48f04b4222a604aaaaaaaa3863 -
```

Die Flag wäre in diesem Fall dann `45c9646a8342d48f04b4222a604aaaaaaaaa3863`.

Natürlich ist dies nicht die wirkliche Flag, es liegt an dir, sie zu finden, James!
Machen wir uns an die Arbeit!

## Boris Maschine

Sie können über SSH in Boris Rechner einsteigen:


- User: `boris`
- Passwort:  `computer`
- Computer: `192.168.0.8`
- Port: `2422`

## Einige nützliche Befehle

- Kopieren einer Datei: `cp Ursprungsdatei Zieldatei`.
- Löschen einer Datei: `rm Datei`.
- Kopieren eines Verzeichnisses: `cp -R Ursprungsverzeichnis Zielverzeichnis`.
- Erstellen eines Verzeichnis: `mkdir Verzeichnis`.
- Löschen eines Verzeichnisses: `rmdir Verzeichnis`. Dies funktioniert nur, wenn das Verzeichnis leer ist.

Vergiss nicht die Geschichten mit den absoluten oder relativen Pfaden. Du kannst Datei- oder Verzeichnisnamen angeben, indem du absolute oder relative Pfade angibst. Vgl. Herausforderung Unix 1 / Golden Eye.

## Versteckte Dateien und Verzeichnisse

Hallo James, hier spricht Q!

Wenn man unter Unix den `ls`-Befehl ausführt, sieht man keine Dateien und Verzeichnisse, deren Namen mit einem `.` beginnen. Wenn eine Datei zum Beispiel `.irgendwas` heißt, werden wir sie nicht sehen.

Um sie zu sehen, müssen Sie dem Befehl `ls` einen Parameter angeben: er ist `-a`. Also, `ls -a` erlaubt Ihnen, alle Dateien in einem Verzeichnis zu sehen...

Ich sage nur...
