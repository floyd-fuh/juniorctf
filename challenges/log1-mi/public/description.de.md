Title: Mission (Im)possible
Category: Unix
Score: 100
Description:

Ethan, wir haben erfahren, dass das Syndikat erneut Angriffe gegen unser Land plant, und zwar gegen unsere Organisation, den IMF (Impossible Missions Force).

Wir haben das Login des Kopfs des Syndikats sichergestellt. Es liegt an Ihnen, herauszufinden, was er vorhat...

## Zugang zum Gerät

- Port: 2922
- Benutzer: `bonedoc`
- Kennwort: `synd1cate`.

## History, ein Keylogger ohne Programmieraufwand

Ich glaube nicht, dass es notwendig ist Sie daran zu erinnern, Ethan, dass unter Unix das, was Sie in Ihre Shell eingeben, in einer *historischen* Liste gespeichert wird. Auf diese kann mit dem Befehl 'history' zugegriffen werden.

Beispiel: 

```
$ history
...
  539  make build
  540  make build
  541  make build
  542  make run
```

Es ist sogar sehr praktisch, denn Sie können einen Befehl aus der History aufrufen, indem Sie `!num` eingeben, wobei Sie `num` durch die Nummer in der History ersetzen.
Wenn wir beispielsweise im obigen Beispiel `!542` tippen, wird `make run` angezeigt.
Wenn Sie `!539` tippen, wird `make build` angezeigt. Dies ist besonders praktisch für lange Befehle.
