Title: Der Boris Passwortfall
Category: Passwords
Score: 100
Description:

James, dieser Idiot Boris Grishenko hat ein weiteres seltsames Passwort eingegeben, um auf das Steuerungssystem von Golden Eye zuzugreifen. Jede Minute zählt... Sie müssen das Passwort herausfinden!

Das Passwort beginnt mit dem Titel eines James Bond Films (auf Englisch, in Kleinbuchstaben und ohne Leerzeichen, wenn es zum Beispiel Golden Eye wäre, wäre es `goldeneye`), gefolgt von **zwei** Ziffern.

## Vorsicht

1. Diese Übung ist schwieriger als `John 2`. Ich empfehle dir, zuerst John1 und John2 zu machen.
2. Und weißt du was? Nein, Passwörter auf Maschinen die nicht dir gehören zu knacken ist nicht cool, es ist gegen das Gesetz...


## Zugriff auf das Steuersystem

Du greifst über SSH auf das System zu:

- port `2222`
- Benutzername: `boris`
- die Server-Adresse lautet `192.168.0.8`.

## Passwörter mit John brechen

Man muss fast die gleiche Technik wie bei John 2 anwenden.
Nur, dass ich dir diesmal nicht die `john.conf'-Datei zur Verfügung stelle. Das musst du selbst tun. Du modifizierst die Datei, die du in john2 erstellt hast, damit sie für diesen Fall funktioniert. Es ist nicht sehr kompliziert, du kannst es machen. England zählt auf dich, James!

Ah! Falls John Ihnen etwas wie `Ungültige Regel in /etc/john/john.conf` sagt, bedeutet das, dass er Ihre Konfigurationsdatei nicht ganz versteht. Du musst einen kleinen Fehler gemacht haben. Bedenke insbesondere, die Anführungszeichen ein wenig zu verschieben... Viel Glück 007!


Übersetzt mit www.DeepL.com/Translator (kostenlose Version)
