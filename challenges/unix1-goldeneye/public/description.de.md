Title: Launching Golden Eye
Category: Unix
Score: 50
Description:

Für Boris Grishenko dreht sich das ganze Leben um Computer. Und alle Informationen, die er sich merken muss, befinden sich auf seinem Computer.
Glücklicherweise hat sich 007 gerade Boris Laptop geschnappt und ihn der Abteilung Q übergeben.

Du, du arbeitest für Q und wir möchten, dass du wichtige Daten in Boris Konto findest. Die Informationen sind in Dateien auf dem Computer versteckt.
Du bekommst Zugang zu Boris Laptop, aber du musst herausfinden wo die geheimen Daten sind...

## Anmelden an Boris Laptop

Benutze SSH:

- User: `boris`
- Password: `computer`
- Computer: `IP ADDRESS`
- Port: `2322`

## Dateien und Verzeichnisse

### Ok, ich will das nicht alles lesen, ich will die Lösung finden!

Ähem. Um die Lösung zu finden, musst du wissen, wie man durch Verzeichnisse und Dateien geht.
Sobald du weisst:

- wie man von einem Verzeichnis in ein anderes wechselt
- eine Datei von einem bestimmten Ort liest

wirst du in der Lage sein, diese Herausforderung zu lösen.

Die nächsten Abschnitte helfen dir dabei, dies zu lernen. Wenn du das schon weisst, freue dich, ... löse die Herausforderung!

### Baum

Unter Unix werden alle Dateien in einem Baum von Verzeichnissen gehalten. Es ist ein bisschen wie ein echter Baum, mit Wurzeln, einem Stamm und Blättern. Unter Unix sind die Wurzel und der Stamm gleich und entsprechen einem Verzeichnis namens `/`, auch "root" genannt.

Dann hat man große Äste. Das sind die ersten Verzeichnisse wie `/etc` oder `/home`.  Das bedeutet "innerhalb des etc Verzeichnisses von root".
Dann haben wir kleinere Zweige: Das sind Unterverzeichnisse Ihres Verzeichnisses. Beispiel `/home/susan`. Das bedeutet "im Verzeichnis susan, das sich innerhalb des Verzeichnisses home von root aus befindet". Okay?

### Pfad

Es gibt zwei verschiedene Möglichkeiten einen Pfad zu beschreiben, d.h. den Weg zu einem bestimmten Dateiverzeichnis.

- **absoluter Pfad**. Dieser erklärt, wie man zu dieser Datei oder diesem Verzeichnis **von root** aus geht. Beispiel: `/home/susan/blah.txt`. **Ein absoluter Pfad beginnt immer mit einem /**.

- **relativer Pfad**. Dieser erklärt, wie man zu dieser Datei/diesem Verzeichnis *von dem Ort aus, an dem man sich gerade befindet*, gelangt. Nehmen wir an, du bist in `/home/susan`. Du möchtest in `/etc` wechseln. Dazu musst du zuerst in das Verzeichnis zurückgehen, das `susan` enthält. Das ist `home`. Dann gehst du von `home` zurück zum Wurzelverzeichnis. Vom Wurzelverzeichnis gehst du zu `/etc`. Ein relativer Pfad **beginnt nie mit /**.

Unter Unix heißt das Verzeichnis, in dem Sie sich gerade befinden, (Abkürzung): `.` (Punkt).
Das Verzeichnis darüber, ist Ihr "Vater" und heißt (Abkürzung): `..` (zwei Punkte).

Stellen wir uns vor, wir befinden uns in einem Verzeichnis namens boris und haben den folgenden Baum:

```
./boris/
|-------- Dockerfile
|-------- john.conf
|-------- public
|         |---- description.md
|-------- words
```
Vom Verzeichnis `boris` aus ist der relative Pfad zu `description.md`: `./public/description.md`.
Vom Verzeichnis `public` aus ist der relative Pfad zu `john.conf`: `../john.conf`.
                                                                                                                                         Aus dem Verzeichnis `boris`, wer ist `././././././Dockerfile` ?
Denk nach. `.` bedeutet das aktuelle Verzeichnis. Wir werden also nicht in ein anderes Verzeichnis wechseln und innerhalb von `boris` bleiben. Dies bezeichnet also die Datei `Dockerfile`. Übrigens, manchmal schreiben wir statt `./Dockerfile` (was klar und präzise ist) einfach nur `Dockerfile` (was weniger präzise ist, aber normalerweise lässt der Kontext erahnen, dass es sich um `./Dockerfile` handelt).

Was ist der Unterschied zwischen `./john.conf`, `john.conf` und `/john.conf`. Vorsicht...

- `./john.conf` bedeutet die Datei `john.conf` im aktuellen Verzeichnis.
- `john.conf` bedeutet genau dasselbe. Das ist die gleiche Datei.
- `/john.conf` beginnt mit `/`. Es ist ein absoluter Pfad. Das ist die Datei `john.conf` im Hauptverzeichnis. Dies ist wahrscheinlich eine ganz andere Datei (es sei denn, Sie befinden sich gerade im Hauptverzeichnis ;)

### In ein anderes Verzeichnis wechseln

Um in ein anderes Verzeichnis zu wechseln, verwenden Sie den Befehl `cd` ("change directory" was Englisch ist für "wechsle Verzeichnis"), gefolgt vom Namen des Verzeichnisses, zu dem wir gehen wollen. Du gibst einen Pfad zu diesem Verzeichnis an, entweder absolut oder relativ.

Beispiel für einen absoluten Pfad: `cd /etc`
Beispiel für einen relativen Pfad: `cd ./etc`.

Vorsicht, denn diese beiden Befehle können dich unter Umständen in unterschiedliche Verzeichnisse bringen. Der zweite Befehl bringt dich in das Unterverzeichnis `etc`, des Verzeichnisses in dem du dich gerade befindest. Wenn du in `/home/susan` bist, gehst du zu `/home/susan/etc`.

### Den Inhalt einer Datei lesen

Verwende den Befehl `ls`, gefolgt vom Pfad (absolut oder relativ) des Verzeichnisses, das du inspizieren willst.

Beispiel:

```
$ ls /usr
bin include lib32 local share x86_64-linux-gnu
games lib lib64 sbin src
```

Der Befehl `ls` hat viele Optionen. Wir werden nicht alle im Detail beschreiben, aber `ls -lh` wird sich als nützlich erweisen. Übrigens, wenn du das Verzeichnis nicht angibst, bedeutet das, dass das aktuelle Verzeichnis aufgelistet wird.

Beispiel: 

```
$ ls -lh
total 24K
drwxr-xr-x 3 axelle axelle 4,0K juil.  7 22:53 boris
-rwxr--r-- 1 axelle axelle  116 juil.  7 22:44 down.sh
drwxr-xr-x 3 axelle axelle 4,0K juil.  6 19:32 john1
drwxr-xr-x 3 axelle axelle 4,0K juil.  7 22:26 john2
drwxr-xr-x 3 axelle axelle 4,0K juil.  8 18:26 unix1
-rwxr--r-- 1 axelle axelle  176 juil.  7 22:44 up.sh
```

Beachte dass: 
- Zeilen, die mit einem klein geschriebenen `d` beginneni anzeigen, dass es sich um ein Verzeichnis handelt (Name am Ende rechts) 
- die anderen Zeilen sind Dateien (Dateiname am Ende rechts).

Boris, John1, John2 und Unix1 sind also Verzeichnisse, während down.sh und up.sh Dateien sind.   

### Den Inhalt einer Datei auslesen
Dazu gibt verschiedene Möglichkeiten, z.B ist `emacs` ein vollständiger Texteditor, aber es geht auch schneller. Du kannst einen Befehl namens `cat` benutzen. Du kannst dem Befehl einen absoluten oder relativen Pfad angeben.

Beispiel:
``` 
$ cat ./host.conf 
# Die "order"-Zeile wird nur von alten Versionen der C-Bibliothek verwendet.
order hosts,bind
multi on
```

Wenn die Datei lang ist, ist der Befehl `more` zu bevorzugen, der die Datei seitenweise anzeigt.
Wenn du dir den Inhalt eines Programms (das auch als ausführbares Programm bezeichnet wird) anschaust, wird dies viele seltsame Zeichen anzeigen, weil Computer nicht die gleiche Sprache sprechen wie wir!
Wenn du versuchst, den Inhalt eines Verzeichnisses zu lesen, wird dir `cat` sagen, dass dies nicht möglich ist.
```
$ cat /etc
cat: /etc: Is a directory
```
