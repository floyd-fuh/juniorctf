Title: Le garage de Pico
Category: Passwords
Score: 100
Description:


Pico, comme tu le sais peut etre, aime bien les belles voitures. Comme son pote, Albert (de Monaco), il a un garage rempli de belles voitures. 

Toi, tu es un vilain petit cambrioleur. Et tu voudrais bien lui ouvrir son garage et lui piquer une de ses voitures, pour faire un petit tour avec. 

Seulement, voilà. Le garage de Pico est protégé par un mot de passe.

Lors d'une soirée un peu trop arrosée (ça veut dire que Pico avait trop bu), Pico a donné des indices. Le mot de passe de son garage est fait du nom d'une marque de voitures, encadré par un chiffre avant et un chiffre après.
Donc du genre, `1peugeot2`. Sauf que Pico n'est pas du genre à mettre Peugeot dans son mot de passe.

## Accéder au garage de Pico

Tu y accèdes par SSH:

- port `2622`
- nom d'utilisateur: `pico`
- l'adresse du serveur est `192.168.0.8`.

## Methode

Tu vas devoir :

- constituer une liste de marques de voiture
- modifier le fichier `john.conf`. Tu peux te servir du fichier de John 2 comme base.
- lancer le programme John. Si tu ne sais plus commment faire, rafraichis toi la mémoire avec John 2.

## Modifier john.conf

Regarde [ici](https://countuponsecurity.files.wordpress.com/2016/09/jtr-cheat-sheet.pdf) la fiche d'aide qu'un internaute a faite (j'espère qu'il n'a pas réussi à ouvrir le garage de Pico avant toi !).

A un endroit, tu trouves la phrase suivante:

```
Add # to the beginning and end of the word
A0"#"Az"#"
```

Ca veut dire que si tu utilises cette règle avec le fichier de marques de voiture, John va tester les mots de passe suivants:

```
#peugeot#
#citroen#
#renault#
...
```

Or, toi, tu ne veux pas encadrer par `#` mais par un chiffre entre 0 et 9. 
A toi de trouver comment modifier.

Ah ! Si John te dit quelque chose comme `Invalid rule in /etc/john/john.conf`, cela veut dire qu'il ne comprend pas tout à fait ton fichier de configuration. Tu as du faire une petite erreur. 


