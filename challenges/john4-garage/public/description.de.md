Title: Picos Garage
Category: Passwords
Score: 100
Description:

Pico mag schöne Autos. Viele schöne Autos. Wie sein Kumpel Albert (aus Monaco), hat auch er eine Garage voller schöner Autos.

Du bist ein ungezogener kleiner Einbrecher. Und du würdest gerne seine Garage aufmachen und eines seiner Autos stehlen, um eine kleine Fahrt damit zu machen.

Aber die Sache ist die, Picos Garage ist kennwortgeschützt....

Auf einer Party, wo alle ein wenig zu viel geredet haben (das heißt, Pico hatte zu viel geredet), gab Pico einige Hinweise. Das Passwort für seine Garage besteht aus dem Namen einer Automarke, umrahmt von einer Zahl davor und einer Zahl danach.
Also so etwas wie `1peugeot2`. Nur dass Pico nicht der Typ ist, der Peugeot als sein Passwort wählt.

## Zugang zu Picos Garage

Du greifst über SSH zu:

- Port 2622.
- Benutzername: `pico`
- die Server-Adresse lautet `192.168.0.8`.

## Methode

Das musst du tun:

- eine Liste von Automarken zusammenstellen
- die Datei `john.conf` modifizieren. Du kannst die John2-Datei als Grundlage verwenden.
- Führt das Programm John aus. Wenn du nicht weißt, wie du das machen sollst, frische dein Gedächtnis mit John2 auf.

## john.conf modifizieren

Schau [hier](https://countuponsecurity.files.wordpress.com/2016/09/jtr-cheat-sheet.pdf) auf das Hilfeblatt, das ein Benutzer von John erstellt hat (ich hoffe, er hat es nicht geschafft, Picos Garage vor dir zu öffnen!)

An einer Stelle findest du den folgenden Satz:

```
Add # to the beginning and end of the word
A0"#"Az"#"
```

Das heißt, wenn du diese Regel mit der Automarken-Datei verwendest, wird John die folgenden Passwörter testen:

```
#peugeot#
#citroen#
#renault#
```

Aber du willst es nicht mit `#' umrahmen, sondern mit einer Zahl zwischen 0 und 9.
Es liegt an dir, herauszufinden, wie du sie modifizieren kannst.

Übrigens... Wenn John etwas wie `Invalid rule in /etc/john/john.conf` sagt, bedeutet das, dass er deine Konfigurationsdatei nicht ganz versteht. Du musst einen kleinen Fehler gemacht haben.


