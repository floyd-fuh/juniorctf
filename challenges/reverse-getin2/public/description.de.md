Titel: Sehr geheime Basis 2
Kategorie: Reverse
Punktzahl: 60
Beschreibung:

Diese Herausforderung ist sehr ähnlich zu `getin1`. Lade dir `getin2` herunter, mach es ausführbar und führe es aus. Du solltest wissen, wie man das macht, sonnst schau dir nochmal `getin1` an.

Genauso musst du `strings` verwenden, um das Passwort zu finden. Aber diesmal ist das Passwort etwas länger und es ist auf mehrere Zeilen aufgeteilt.

## Brauchst du Hilfe?

Benutze die Hinweise...

## Flag

Verwende das gefundene Passwort, um diese Herausforderung abzugeben.
