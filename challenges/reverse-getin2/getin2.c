#include <stdio.h>
#include <string.h>

int main(int argc, char **argv) {
  char line[27];
  char pass[] = "KrokodileSindGross";

  printf("=== SUPER GEHEIME BASIS ===\n");
  printf("Passwort: ");

  if (fgets(line, sizeof(line), stdin) !=NULL) {
    line[strcspn(line, "\n")]= 0;
    if (strcmp(line, pass) == 0 ) {
      printf("\n KORREKT!\n");
      printf("Benutze das Passwort als Flag fuer die Challange.\n");
      return 0;
    }
  }

  printf("Zutritt verweigert!\n");
  return -1;
}
