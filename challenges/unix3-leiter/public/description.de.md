Title: Leiter
Category: Unix
Score: 50
Description:

James, England braucht dich! Wir haben von einer möglichen planetarischen Katastrophe erfahren.
Felix Leiter war für die Operation verantwortlich, aber er ist nirgends zu finden...
Wir müssen schnell handeln!

Du wirst auf den Computer von Felix zugreifen, um zu versuchen, die nötigen Informationen zu finden.

Ich hoffe, du hast deine Unix-Lektionen gut verfolgt (und gelernt)! Ansonsten zurück zu *GoldenEye* oder *Exfiltration*.

Viel Glück, James!

## Zugriff auf den Computer von Felix

Verwende SSH:

- User: `leiter`
- Kennwort: `burger4ever`
- Computer: `192.168.0.8`
- Port: `2522`

