Titel: Geheime Basis 1
Kategorie: Reverse
Punktzahl: 50
Beschreibung:


Hallo! Wie geht es dir? Wie wär's, wenn wir langsam *wirklich coole Tricks wie Reversen* anwenden?

Versuche, das Programm `getin` zu knacken. 

- Lade es vom CTF-Server herunter (siehe unten). 
- Gehe in das Verzeichnis, in dem du Datei heruntergeladen hast (erinnerst du dich? benutze den Befehl `cd`).
- Mach die Datei ausführbar:

```
$ chmod u+x ./getin
```

- Führe sie schließlich unter Linux aus:

```
$ ./getin  
=== GEHEIME BASIS ===
Passwort:
```

Jetzt kannst du versuchen, das Passwort zu erraten... Viel Erfolg!

## Keine Ideen mehr?

Manchmal wird das Passwort einfach im Programm selbst geschrieben (versteckt). Wie wäre es mit der Anzeige aller in der ausführbaren Datei enthaltenen Nachrichten? Du kannst dies unter Unix mit einem Befehl namens `strings` tun.

Versuche: `strings ./getin`

Du wirst mehrere Strings(Zeichenketten) erhalten, viele davon sind an dieser Stelle nicht sehr interessant. Aber einige andere Strings sind interessanter. Schaue dir mal diese an:


```
...
Pinguin
=== GEHEIME BASIS ===
Passwort:
KORREKT!
Falsch, geh zurueck in den Kindergarten!
...
```

Wenn das Passwort also weder `Falsch`, noch `Kindergarten`, noch `GEHEIME BASIS` ist, was könnte es sein... :-) Probiere es aus!

## Flag

Verwende das Passwort, um die Herausforderung zu lösen und deine Punkte zu sammeln.
