#include <stdio.h>
#include <string.h>

int main(int argc, char **argv) {
  char line[25];
  char pass[] = "Pinguin";

  printf("=== GEHEIME BASIS ===\n");
  printf("Passwort: ");
  
  if (fgets(line, sizeof(line), stdin) !=NULL) {
    line[strcspn(line, "\n")]= 0;
    if (strcmp(line, pass) == 0 ) {
      printf("\n KORREKT!\n");
      return 0;
    }
  }

  printf("Falsch, geh zurueck in den Kindergarten!\n");
  return -1;
}
