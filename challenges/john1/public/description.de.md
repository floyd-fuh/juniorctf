Title: John 1
Category: Passwörter
Score: 50
Description:

Palpaguin hat gerade den Todesstern fertig gestellt.

Die Rebellen schafften es jedoch glücklicherweise kurz Zugriff auf das System des Todesstern zu erlangen. Sie kopierten wichtige Dateien welche das Passwort von Palpaguin in der sogenannten *hashed* Form enthalten. Das ist nicht das Passwort selbst, aber ist trotzdem hilfreich. Sie wissen ausserdem, dass das Passwort von Palpaguin der Name einer Frucht ist in Kleinbuchstaben.

Hilfst du uns, damit wir uns mit Palpaguin's Zugang anmelden können?
Du kannst dieses *hashed* passwörter am Ende dieser Beschreibung herunterladen.

## Zugriff zum Todesstern Kontrollsystem 

Um zum Kontrollsystem des Todesstern zu verbinden, tippe folgendes Kommando ein: `ssh -p 2022 palpaguin@IP-ADDRESS`

Erklärung:

- ssh ist ein Netzwerkprotokoll das sehr oft benutzt wird, um ein System zu administrieren
- `-p 2022` bedeutet Port Nummer 2022. Ein Port ist ein Kommunikationskanal. Du kannst annehmen, dass ein Port wie ein Türe ist bei einem Haus, das den Zugang zu einem Raum regelt. Nur dass es hier eben eine Computer-Türe ist ;)
- `palpaguin` ist der Benutzername. Sein Name ist ja palpaguin :)
- `192.168.0.8` ist die IP Addresse des Servers. Häuser haben normalerweise eine Adresse und Computer haben eben stattdessen IP Adressen.

Falls der Server eine Antwort wie die folgende gibt, antworte mit "yes" und drücke Enter:
```
The authenticity of host '[127.0.0.1]:2022 ([127.0.0.1]:2022)' can't be established.
RSA key fingerprint is 2d:93:f1:87:f8:91:8f:87:46:63:72:fd:c9:24:ed:d9.
Are you sure you want to continue connecting (yes/no)?
```

Anschliessend fragt dich das System nach einem Passwort. Das ist das Passwort welches du finden möchtest. Das sieht dann so aus:

```
palpaguin@192.168.0.8's password: 
```

Du kannst dann ein Passwort eintippen (man sieht dabei jedoch nicht was du tippst). Wenn du dann Enter drückst, schaut der Server ob das Passwort stimmt.

## Achtung

Das ist ein Spiel. Im echten Leben ist es streng verboten auf den Zugang von jemand anderem zuzugreifen. Das solltest du niemals tun, auch nicht zum Spass oder um Freunde zu beeindrucken. Passwörter zu knacken ist ebenfalls nicht gestattet.

Manchmal kann diese Wissen jedoch nützlich sein. Stell dir vor du hast dein eigenes Passwort auf deinem Computer vergessen! Natürlich darfst du dann das Passwort für deinen eigenen Zugang knacken. Hier ist es ebenfalls gestattet, weil es ein Spiel ist.

## Passwörter knacken mit John The Ripper

Um diese Aufgabe zu lösen, schlag ich vor, dass du ein richtig hilfreiches Werkzeug mit dem Namen "John The Ripper" verwendest.

1. Installiere John. In einem Terminal, tippe `sudo apt-get install john`. Falls das nicht funktioniert, frage einen Erwachsenen.
2. Mache eine neue Datei und schreib alle Früchte Namen rein die du kennst (nur mit Kleinbuchstaben). Auf jeder Zeile der Datei sollte ein Früchtename stehen. Speicher die Datei ab.
3. John The Ripper kann Passwörter für dich testen und schauen, ob eines dem *hashed* Passwort entspricht. Um dies zu tun, starte ein Terminal und tippe `john --wordlist=deinedatei hashedpass`. Natürlich musst du "deinedatei" mit dem Namen der Früchtenamen-Datei ersetzen. Ausserdem musst du "hashedpass" mit dem Namen der Datei ersetzen welche du heruntergeladen hast und welches das *hashed* Passwort von Palpaguin enthält.

