Title: Les voitures de Pico
Category: Passwords
Score: 150
Description:

*Sie müssen das Rätsel von Pico's Garage lösen, bevor Sie diese Aufgabe in Angriff nehmen*

Bravo! Sie sind also in Picos Garage gekommen. Und da, vielleicht ist Ihnen aufgefallen, dass Sie seine Autos nicht benutzen konnten, weil Sie ein *anderes* Passwort brauchten! Verdammt!

Ziel: Leih dir ein Auto von Pico. Es ist egal welches.

## Leih dir ein Auto aus

Um zu einem Auto zu kommen, braucht man:

- die Anmeldung des Fahrzeugs, das den Benutzernamen darstellt. Wie Pico für Pico, oder Boris für Boris. Nur dass dies ein Autoname ist.

- Die Passwörter für dieses Auto. Das finden mit John The Ripper.

## Finden Sie die Anmeldedaten

Gehen Sie in Picos Garage. Sie greifen über SSH darauf zu:

- Port: `2822`
- Benutzer: `pico`
- Maschine: `IP-ADRESSE-DES-COMPUTERS`
- Picos Passwort: Sie haben es im Puzzle von Picos Garage gefunden.

**Sobald Sie in Picos Garage** sind, können Sie die **Logins** der Autos finden, indem Sie den folgenden Befehl ausführen:

```
cat /etc/passwd | cut -d: -f1 | tail -n 3
```

## Mögliche Passwörter

Wir wissen, dass die Passwörter zu den Autos von Pico auf Autoteilen basieren. Ein Reifen ist zum Beispiel ein Teil eines Autos, also könnte es so etwas sein.

Schreib eine Liste von Autoteilen auf. Schreib es in Kleinbuchstaben, keine Umlaute wie ä, ö oder ü, stattdessen a, o oder u.

Du musst diese Liste von Autoteilen irgendwo in Pico's Garage deponieren. Zum Beispiel in Pico's Konto hier: `/home/pico/parts`.

## Vorsicht, es gibt zwei Maschinen: Ihre und Pico's Garage.

Vorsicht bei diesem Rätsel muss man verstehen, dass es zwei verschiedene Maschinen gibt:

- Ihre.
- Pico's Garage (Sie fahren über SSH dorthin).

Dateien gehen nicht magisch von einer Maschine zur anderen! Wenn du eine Datei in Picos Garage schreiben möchtest, musst du in Picos Garage sein. Oder du kopierst die Datei in Picos Garage (wir werden später sehen, wie das geht).


## Ich gebe Ihnen keine John the Ripper Datei!

Diesmal liegt es an Ihnen, die Datei aus der Maschine zu holen.
Führen Sie dazu **den folgenden Befehl aus, wenn Sie in Picos Garage** sind:

```
unshadow /etc/passwd /etc/shadow | tail -n 3 > /tmp/autos
```

Dieser Befehl erzeugt die John the Ripper Datei, nur heißt sie diesmal `/tmp/autos`.

Wenn Sie wollen, können Sie den Inhalt von `/tmp/autos` lesen.

## Konfigurationsdatei

Die Konfigurationsdatei `john.conf` ist bereits in Picos Garage vorhanden. Mach dir keine Sorgen und benutz diese.

## Benutz John The Ripper

In Picos Garage, hast du:

- die Liste der Autoteile
- Die Konfigurationsdatei von John The Ripper an der richtigen Stelle
- die Datei `/tmp/autos`, die zu knacken ist

Du bist bereit, John The Ripper zu starten.

```
john --wordlist=/home/pico/parties --regeln /tmp/autos
```

Wenn du Glück hast, findest du Passwörter!

## Ins Auto einsteigen

Wenn du ein Autopasswort hast, gib es via SSH ein:

- Port: `2822`
- Benutzer: der Name des Autos
- Maschine: `IP-ADRESSE-DES-COMPUTERS`
- Passwort: das gefundene Passwort

So findest du die Lösung.
**Es ist nicht nötig, in jedes Auto zu steigen (es sei denn, es gefällt dir). Ein einziges Auto reicht aus, um die Lösung zu finden und das Rennen zu fahren.**

