Title: Pico's Kühlschrank
Score: 100
Category: Passwords
Description:

Pico hält seinen Champagner kalt und sicher in seinem Kühlschrank.
Aber wir sind gerade von einer langen Wanderung zurück und haben Durst. Wir würden definitiv gerne etwas trinken, aber ... Pico hat seinen Kühlschrank mit einem Passwort geschützt !! Kannst du das glauben? Einen Kühlschrank mit einem Passwort zu schützen. Jedenfalls hat er es geschafft.

Ihre Mission: Öffnen Sie den Kühlschrank und trinken Sie Picos Champagner.

## Öffnen Sie die Tür des Kühlschranks

Der Zugang zur Tür erfolgt über SSH. Erinnerst du dich, wie das geht?

- Name des Kühlschranks: IP ADRESSE
- Port: 2932
- Identifier: Pico

Nun finden Sie das Passwort. Das ist Ihre Aufgabe!

## Erinnerungen für das wirkliche Leben

- Wenn man durstig ist, ist Wasser viel effizienter ;)
- Es ist **verboten**, auf Systeme zuzugreifen, wenn du nicht dazu befugt bist. Wirklich.

## Rohe Gewalt aka. Brute force

Um diese Challenge zu lösen, rate ich dir, die sogenannte "brute force" Methode anzuwenden.

Du könntest viele Passwörter ausprobieren, um den Kühlschrank zu öffnen. Irgendwann würdest du vermutlich das richtige finden. Wenn du aber nicht sehr viel Glück hast, wird dies sehr viel Zeit in Anspruch nehmen.

Daher werden wir ein Programm einsetzen, dass die Arbeit für uns erledigt. Das Programm wird Hunderte von Passwörtern testen. Dieses Programm heißt **Hydra**.

## Hydra

Falls du Kali einsetzt, ist Hydra bereits vorinstalliert. Ansonsten musst du es noch manuell installieren:

```
sudo apt-get install hydra hydra-gtk
```

Schauen wir uns Hydra genauer an.

## Handbuch

Fast alle Unix-Befehle kommen mit "manual pages". Das ist eine Dokumentation, die erklärt, wie man den Befehl oder das Programm benutzt.

Du kannst dir diese mit folgendem Befehl anzeigen lassen:

```
man hydra
```

**man** ist der Unix-Befehl zum Anzeigen einer manual page. Dann gibst du *hydra* ein, um auf die Dokumentation von Hydra zuzugreifen. **Wenn du unter Unix Hilfe zu einem Befehl benötigst, kannst du das immer benutzen.**

Das Handbuch kommt mit einer kurzen Beschreibung:

```
HYDRA(1)                    General Commands Manual                   HYDRA(1)

NAME
       hydra  - a very fast network logon cracker which support many different
       services
```

Dann werden alle möglichen Parameter erläutert.

```
SYNOPSIS
       hydra
        [[[-l LOGIN|-L FILE] [-p PASS|-P FILE|-x OPT]] | [-C FILE]] [-e nsr]
        [-u] [-f] [-F] [-M FILE] [-o FILE] [-t TASKS] [-w TIME] [-W TIME]
        [-s PORT] [-S] [-4/6] [-vV] [-d]
        server service [OPTIONAL_SERVICE_PARAMETER]
```


## Lasst uns loslegen!

Wir wollen hunderte von Passwörtern auf Picos Kühlschrank ausprobieren. Das bedeutet, dass du Hydra mit folgenden Parametern laufen lassen musst:

- einen service. In unserem Fall ist das **ssh**
- einen server. Dies wird **IP ADDRESS** sein.
- einen Benutzernamen. Das ist der Benutzer, mit dem du versuchst, dich anzumelden. In unserem Fall also **pico**.
- Passwörter zum ausprobieren. Ich stelle die Liste der 500 am häufigsten verwendeten Passwörter zur Verfügung. Lade sie herunter und benutze sie mit Hydra.

Der Befehl wird dann so aussehen:

```bash
$ hydra -l pico -P ./500-worst-passwords.txt ADRESSE ssh
```

Eigentlich stimmt der Befehl nicht ganz genau. Du musst den Parameter zur Angabe der **Port nummer** finden, und 2932 als Wert angeben. Aber ich bin sicher, dass du das kannst!

## Falls jemals

Mit Hydra, wenn du diesen Fehler bekommst:

```
[ERROR] ssh protocol error
[ERROR] ssh protocol error
[ERROR] ssh protocol error
[ERROR] ssh protocol error
```

Loggen Sie sich manuell in Picos Kühlschrank ein und versuchen Sie es dann automatisch mit Hydra erneut.
